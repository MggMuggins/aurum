# aurum
An AUR helper written for efficiency, ease-of-use, and no pacman wrapping. Aurum means gold in latin, as well as containing "aur". The name was suggested by @xTibor.

It's absolutely a project born out of frustration with all the other AUR helpers out there, and as a learning experience for writing a package manager in general.

Documentation can be found for the cli tool and configuration file on the [wiki](https://gitlab.com/MggMuggins/aurum/wikis/home).

DISCLAIMER: In its current state, aurum has undergone _very little_ testing. Basically all I've done is use it to resolve my (relatively extensive) required updates on my machine and used it to install a few packages. Since this is effectively all aurum does right now, it's a reasonably effective test of the functionality's surface area, but know that there are bugs, and that I am not responsible if the tool breaks your system.

That said, Aurum is written with :heart: in Rust, so there are maybe less bugs. If you use the tool and it breaks, I am happy to help and to hear what I could do better. This is a tool I want to be able to use with confidence.

# Goals
Note that not all of these are currently being met, aurum is still undergoing development.
 - **Don't wrap `pacman`**: Source package management is fundamentally different than binary package management, and therefore using the same cli interface for both uses doesn't make sense (Read: I don't like pacman-wrappers; It's a control thing).
 - **Pacman-esque CLI**: That said, pacman's cli is surprisingly robust and flexible. No other AUR helper that I know of uses a similar CLI (unless it's wrapping pacman).
 - **Relatively efficient**: I don't want the user to be sitting and waiting on dependency resolution. Using rust helps with this.
 - **Featureful**: I want this thing to be able to do everything the more popular AUR helpers can, that includes updating local AUR packages, and resolving dependencies. I can also leverage some of the more obscure options and functionality of `makepkg`.
 - **Use of `sudo`**: Don't run as root. (Look in `main.rs` under the logger init for a laugh)

# TODO
 - [x] Implement dependencies for most things (doesn't really leverage the resolver right now)
 - [x] Better configuration file stuff (could implement more options)
 - [ ] Better output/user interaction. Possibly read pacman's config file for some options?
 - [x] Update
 - [ ] Search functionality

# Limitations (Current)
- There are bugs, I know that this code is not infallible, especially considering all the things that I've (re)implemented. Please report them if you find them.
- Update doesn't offer options for packages to update. This could be fixed with a number interface, but would also require some structural changes.
- Dependency resolution is sorta slow. My database load functions are not optimized, and there is no easy way to tell which packages I need from the AUR at once, so it's not easy to reduce the number of `info` requests to the AUR.
- There are literally no tests. I need to write tests...
