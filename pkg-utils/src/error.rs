use std::result::Result as StdResult;

use failure::{Error, Fail};

//TODO: There is probably a more idomatic and sensical
// way to do error handling with Failure. Need to do that.
pub(crate) type Result<T> = StdResult<T, Error>;

// All u32's are the line numbers
#[derive(Display, Debug)]
pub enum ParseError {
    // The string is the key
    #[display(fmt = "{}: too many instances of {}", _0, _1)]
    MultipleKeys(u32, String),
    
    #[display(fmt = "{}: missing a key", _0)]
    MissingKey(u32),
    
    // The string is the key
    #[display(fmt = "{}: missing value for key {}", _0, _1)]
    MissingValue(u32, String),
    
    #[display(fmt = "{}: unknown key {}", _0, _1)]
    UnknownKey(u32, String)
}

impl Fail for ParseError {}
unsafe impl Send for ParseError {}
unsafe impl Sync for ParseError {}
