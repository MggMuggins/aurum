mod cache;

use std::fmt::Debug;
use std::fs::{create_dir_all, File, read_dir};
use std::io::Read;
use std::path::{Path, PathBuf};

use failure::err_msg;
use flate2::read::GzDecoder;
//use itertools::{Itertools, process_results};
use pkg_utils::PkgSpec;
use reqwest;
use tar::Archive;
use toml;
use version_compare::Version;

use self::cache::PkgInfoCache;
use util::{Command, InstallReason, Pacman, Result, xdg_dir};

/// Configuration for Aurum. Note that paths in the toml should really
/// be canonical, Aurum will get really confused really fast and might
/// have undefined behavior.
/// This thing encapsuates a configuration, including the packages and their
/// states contained within, and actions that can be applied to this config.
#[derive(Deserialize)]
pub struct Config {
    pub src_dir: Option<PathBuf>,
    pub build_dir: PathBuf,
    pub pkg_cache_dir: Option<PathBuf>,
    #[serde(skip)]
    pub cache: PkgInfoCache
}

impl Config {
    /// Parses a config file or uses the default
    pub fn new(config_file: Option<impl AsRef<Path>>) -> Result<Config> {
        let config = if let Some(config_file) = config_file {
            debug!("reading config file: {:?}", config_file.as_ref());
            
            let mut toml = String::new();
            let mut config_file = File::open(config_file)?;
            config_file.read_to_string(&mut toml)?;
            toml::from_str(&toml)?
        } else {
            debug!("using default configuration");
            Config::default()
        };
        config.check_dirs()?;
        
        /* All this initializes the cache with some packages which are likely to
        //   be used. I'm sorta doubting the wisdom of doing this.
        // The built packages
        let all_built = config.all_built_pkgs()?;
        let all_built = all_built.iter()
            .map(|package_path| {
                let (_path, spec) = get_pkgspec(package_path);
                spec
            })
            .collect_vec();
        
        if all_built.iter().any(|opt| opt.is_none() ) {
            return Err(err_msg("some build package's name can't be parsed"));
        }
        
        let need_info = all_built.iter()
            .map(|opt| opt.unwrap().name )
            .collect_vec();
        
        // And the packages that have build directories
        for entry in read_dir(&config.build_dir)? {
            let entry = entry?;
            if entry.file_type()?.is_dir() {
                let pkg_name = String::from(entry.file_name().to_string_lossy());
                need_info.push(pkg_name);
            }
        }
        // Considering not doing this
        config.cache.add(need_info);
        */
        Ok(config)
    }
    
    /// Makes sure that the configuration directories exist, and creates
    /// them if they don't.
    fn check_dirs(&self) -> Result<()> {
        create_dir_all(&self.build_dir)?;
        debug!("build_dir exists: {:#?}", self.build_dir);
        
        if let Some(src_dir) = &self.src_dir {
            create_dir_all(src_dir)?;
            debug!("src_dir exists: {:#?}", src_dir);
        }
        
        if let Some(pkg_cache_dir) = &self.pkg_cache_dir {
            create_dir_all(pkg_cache_dir)?;
            debug!("pkg_cache_dir exists: {:#?}", pkg_cache_dir);
        }
        Ok(())
    }
    
    /// Returns a list of all packages that have been built for this aurum configuration.
    /// This doesn't read self.cache
    fn all_built_pkgs(&self) -> Result<Vec<PathBuf>> {
        let packages = if let Some(ref pkg_cache_dir) = &self.pkg_cache_dir {
            // This assumption is being made everywhere, so I guess here too
            get_pkgs(pkg_cache_dir)?
        } else {
            let mut packages = Vec::new();
            for build_dir in read_dir(&self.build_dir)? {
                let mut pkgs = get_pkgs(build_dir?.path())?;
                packages.append(&mut pkgs);
            }
            packages
        };
        trace!("all built pkgs: {:?}", packages);
        Ok(packages)
    }
    
    /// Get the highest version of a package that has been built
    fn newest_built(&self, pkg_name: impl AsRef<str>) -> Result<Option<PathBuf>> {
        let mut all_built = self.all_built_pkgs()?;
        let pkg_path = all_built.drain(..)
            .map(get_pkgspec)
            .filter(|(_, pkgspec)| &pkgspec.name == pkg_name.as_ref() )
            .max_by(|(_, pkg1), (_, pkg2)| {
                // Assume that archlinux package versions can be understood
                // by version-compare <- Idk what to do here
                let ver1 = pkg1.version_str();
                let ver2 = pkg2.version_str();
                let pkg1 = Version::from(&ver1).expect(&format!("Invalid version: {}", ver1));
                let pkg2 = Version::from(&ver2).expect(&format!("Invalid version: {}", ver2));
                // `compare` only returns Orderings that can be converted to `Ordering`
                // That was the case when I read the source
                pkg1.compare(&pkg2).ord().expect("unreachable")
            })
            .map(|(filename, _)| filename );
        Ok(pkg_path)
    }
    
    pub fn is_pkg_up_to_date(&self, pkg_name: String) -> Result<bool> {
        let pkg_path = self.newest_built(&pkg_name)?;
        
        if let Some(pkg_path) = pkg_path {
            let (_, spec) = get_pkgspec(pkg_path);
            
            let pkg_info = self.cache.get(pkg_name)?;
            // If these panic, it means that the versions are not proper version
            // strings (has no number part or no parts, see version-compare source)
            // AKA: based on source code, these are *probably* fine
            let built_version = spec.version_str();
            let built_version = Version::from(&built_version)
                .expect(&format!("Invalid version: {}", built_version));
            let info_version = Version::from(&pkg_info.version)
                .expect(&format!("Invalid version: {}", &pkg_info.version));
            Ok(built_version >= info_version)
        } else {
            Ok(false)
        }
    }
    
    /// Fetch a series of PKGBUILD's based on a [`Config`](struct.Config.html).
    /// This function will make an `info` request to the AUR and then download
    /// the tarballs with the PKGBUILD's for each package. These will be unpacked into
    /// `build_dir` in the provided config.
    /// This function takes an array of packages in order to minimize AUR requests
    //TODO: This function needs work
    pub fn fetch(&self, pkgs: Vec<String>) -> Result<()> {
        debug!("fetch targets: {:?}", pkgs);
        // Make this a little more efficient
        self.cache.add(&pkgs)?;
        
        for pkg in pkgs.iter().cloned() {
            let pkg = self.cache.get(pkg)?;
            
            info!("unpacking {}.tar.gz in {:#?}", pkg.name, self.build_dir);
            let tarball_url = reqwest::Url::parse(raur::AUR_URL)?
                .join(&pkg.url_path)?;
            let tarball = reqwest::get(tarball_url)?;
            
            Archive::new(GzDecoder::new(tarball))
                .unpack(&self.build_dir)?;
        }
        Ok(())
    }
    
    /// Build a package from an aurum configuration and package name. Iteration
    /// for building multiple packages is pushed out to userland.
    /// `package_name` should be only the package name, not a path!
    pub fn build(&self, package_name: impl AsRef<Path>) -> Result<()> {
        let pkgbuild_dir = Path::join(&self.build_dir, &package_name);
        debug!("building {:#?} in: {:#?}", package_name.as_ref(), pkgbuild_dir);
        
        let srcdir = match &self.src_dir {
            Some(srcdir) => srcdir,
            None => Path::new("")
        };
        let pkgdir = match &self.pkg_cache_dir {
            Some(pkgdir) => pkgdir,
            None => Path::new("")
        };
        
        Command::new("makepkg")
            .arg("-f")
            .current_dir(pkgbuild_dir)
            .env("SRCDEST", srcdir)
            .env("PKGDEST", pkgdir)
            .run()
    }
    
    /// Find the latest built package for a given package name and install that
    /// package using `pacman -U`. This is a no-op if package_names is empty.
    pub fn install<S: AsRef<str> + Debug>(&self, package_names: &[S], reason: InstallReason) -> Result<()> {
        debug!("install targets: {:?}", package_names);
        
        if package_names.len() > 0 {
            let mut targets = Vec::new();
            for package_name in package_names.iter() {
                if let Some(pkg_filename) = self.newest_built(&package_name)? {
                    targets.push(pkg_filename);
                } else {
                    let msg = err_msg(format!("package not built: {:#?}", package_name));
                    return Err(msg);
                }
            }
            
            Pacman::install_pkgs(&targets, reason)
        } else {
            Ok(())
        }
    }
}

impl Default for Config {
    /// Do not use this implementation outside this module
    fn default() -> Config {
        let mut cache_dir = xdg_dir("XDG_CACHE_HOME", ".cache");
        cache_dir.push("aurum");
        Config {
            src_dir: None,
            build_dir: cache_dir,
            pkg_cache_dir: None,
            cache: PkgInfoCache::default()
        }
    }
}

/// Get all the alpm packages in a directory
fn get_pkgs(directory: impl AsRef<Path>) -> Result<Vec<PathBuf>> {
    let mut packages = Vec::new();
    for pkg_path in read_dir(&directory)? {
        let pkg_path = pkg_path?.path();
        if pkg_path.to_string_lossy().ends_with(".pkg.tar.xz") {
            packages.push(pkg_path);
        }
    }
    Ok(packages)
}

/// Passes back ownership of the original path
/// Can panic if `path` is not what it's expecting
//TODO: Maybe don't do this ^^
fn get_pkgspec(pkg_path: PathBuf) -> (PathBuf, PkgSpec) {
    let spec = {
        let filename = pkg_path.file_name().unwrap().to_string_lossy();
        let file_stem = filename.trim_end_matches(".pkg.tar.xz");
        PkgSpec::split_pkgname(file_stem)
            .expect(&format!("Invalid PkgSpec: {:?}", pkg_path))
    };
    (pkg_path, spec)
}
